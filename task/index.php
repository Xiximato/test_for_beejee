<?php

//FRONT CONTROLLER

//1.Объявление всех переменных
define('APPLICATION',dirname(__FILE__)."\\application");//Переменная пути пользовательских файлов
define('SYSTEM',dirname(__FILE__)."\\system");//Переманная пути системных файлов
define('BASEPATH',dirname(__FILE__));//Переменная безопасности файловы

//2.Подключение файлов системы
require_once(SYSTEM."\\libraries\\loader.php");//Подключение автозагрузчика
require_once(SYSTEM."\\libraries\\develop.php");//Подключение файла разработчика

//3.Вызов Router
$router = new Router_System();//Создание экземпляра класса
$router->run();//Запуск приложения
