<?php
//Проверяем существование переменной, которая объявляется в index.php
defined('BASEPATH') or die("Вот так работает защита от запроса на все файлы кроме корневого index.php");

//Отображение ошибок
ini_set('display_errors',1);
error_reporting(E_ALL);

//Функция корректного отображения данных
function debug($str){
  echo "<pre>";
  print_r($str);
  echo "</pre>";
}
