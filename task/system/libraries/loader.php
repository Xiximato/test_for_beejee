<?php
//Проверяем существование переменной, которая объявляется в index.php
defined('BASEPATH') or die("Вот так работает защита от запроса на все файлы кроме корневого index.php");

//Сделал кое-как так-как ещё не смог до конца разобраться как
//работает этот магический autoload
spl_autoload_register(function($class){
  $dir_core = SYSTEM."\\core";
  $dir_controllers = APPLICATION."\\controllers";
  $dir_model = APPLICATION."\\models";
  $dir_helper = APPLICATION."\\helper";
  $path = explode("_",$class);
  switch (end($path)) {
    case 'Model':
    require_once($dir_model."\\".$class.".php");
    break;
    case 'Controller':
    require_once($dir_controllers."\\".$class.".php");
    break;
    case 'System':
    require_once($dir_core."\\".$class.".php");
    break;
    case 'Helper':
    require_once($dir_helper."\\".$class.".php");
    break;
  };
});
