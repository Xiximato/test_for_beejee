<?php
//Проверяем существование переменной, которая объявляется в index.php
defined('BASEPATH') or die("Вот так работает защита от запроса на все файлы кроме корневого index.php");

/**
 *Основная задача класса это реализовать подключение к базе данных
 *И при вызове моделью контроллера обеспечить связь с базой данных
 */
class Model_System{

  public $db;//Переменная, которая хранит в себе класс подключения к бд

  function __construct(){
    $this->db = new DataBase_System;//Создание экземпляра класса
  }
}
