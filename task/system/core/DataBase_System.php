<?php
//Проверяем существование переменной, которая объявляется в index.php
defined('BASEPATH') or die("Вот так работает защита от запроса на все файлы кроме корневого index.php");

/**
 *Класс предназначенный для соединения с базой данных
 */
class DataBase_System{

  public $link;//Переменная подключения

  function __construct(){
    $option = require APPLICATION."\\config\\database.php";//Конфигурационный файл подключения

    //Создание подключения
    $this->link = mysqli_connect($option['host'],$option['username'],$option['password'],$option['dbname']);
  }

  //Самый простой запрос который возвращает результат выполнения запроса
  public function query($sql){
    return mysqli_query($this->link,$sql);
  }

  //Тут ещё можно прописать кучу функций которые
  //возвращают разные форматы данных от строчек, до массивов и т.д.
}
