<?php
//Проверяем существование переменной, которая объявляется в index.php
defined('BASEPATH') or die("Вот так работает защита от запроса на все файлы кроме корневого index.php");

/**
 *Класс который наследуют все контроллеры
 */
class Controller_System{

  public $view;//Переменная в которой храниться класс view.

  function __construct($name){
    $this->view = new View_System($name);//Теперь из любого контроллера можно обратиться к классу View
  }

  //Функция прогрузки подели
  function load_model($model){
    $name = ucfirst($model)."_Model";//Формируем имя модели
    $this->$model = new $name;//и создаём экземпляр её класса
    //Теперь из любого контроллера можно обратиться к модели которую подгрузили в нём
  }

}
