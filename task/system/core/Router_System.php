<?php
//Проверяем существование переменной, которая объявляется в index.php
defined('BASEPATH') or die("Вот так работает защита от запроса на все файлы кроме корневого index.php");

/**
 * Основная задача класса распарсить получаемый uri и направить пользователя в необходимое место
 */
class Router_System{

  private $base;//Переменная корневого пути
  private $routes;//Переменная всех путей

  //Первичная функция распределяющая все переменные
  function __construct(){
    session_start();//Запуск сессии
    $this->routes = require_once(APPLICATION."\\config\\routes.php");//подключение файла путей
    $this->base = array_shift($this->routes);//Забираем базовый url(Папка в которой находится точка входа)'task'
    define('ROOT',$this->base);//Объявляем переменную для повсеместного использования
  }

  //Функция для нахождения совпадений между запросом и доступными путями
  public function match(){
    //Отрезаем базовый url, чтобы не мешался, и сразу парсим на составляющие path и query
    $uri = parse_url(preg_replace("/\/".$this->base."\//","",$_SERVER['REQUEST_URI']));
    foreach ($this->routes as $route => $value) {//Ищем совпадения
      if(preg_match('#^'.$uri['path'].'$#',$route)){
        $controlls['controller'] = $value[0];
        $controlls['action'] = $value[1];
        isset($uri['query'])?parse_str($uri['query'],$controlls['params']):[];
        return $controlls;
        //При найденых совпадениях создаём и заполняем массив
        //'controller' - вызываемый котроллер
        //'action' - вызываемый метод
        //'query' - пришедшие параметры
      }
    }
    return false;//Если за работу цикла не было найдено совпадений значит все плохо
  }

  public function run(){
    $segments = $this->match();//Принимаем полученный массив в переменную
    //Здесь её нужно проверить и при false вывести страницу с кодом ошибки

    $name = array_shift($segments);//Забираем из массива имя контроллера
    //Переменная послужит для формирования имени модели и контроллера
    $controller = ucfirst($name)."_Controller";//Формируем имя контроллера к которому будем обращаться
    $controllerAction = array_shift($segments)."Action";//Формируем имя метода который будем вызывать
    $controllerParams = array_shift($segments);//Формируем переменную передаваемых параметров

    $controllerObject = new $controller($name);//Создаём экземпляр класса
    $controllerObject->$controllerAction($controllerParams);//Вызываем метод класса с параметрами
  }
}
