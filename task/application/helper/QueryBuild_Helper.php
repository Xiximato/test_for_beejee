<?php
//Проверяем существование переменной, которая объявляется в index.php
defined('BASEPATH') or die("Вот так работает защита от запроса на все файлы кроме корневого index.php");

/**
 * Вспомогательный класс чтобы быстро формировать url для компонентов
 */
class QueryBuild_Helper{

  //Формирование url на сортировку
  //На вход приходят две переменных
  //Одна из них предидущие параметры
  //Вторая, новый параметр
  public static function buildUrlOnHead($params,$field){
    //Основные параметры сортировки, то на какой странице находимся
    //По какому поля происходит сортировка
    //В каком порядке
    //Если поле сортировки есть и оно совпадает с полем которое нужно
    if(isset($params['sort']) and $params['sort']==$field){
      //Если поле последовательности ести и оно совпадает с возврастание
      if(isset($params['sequence']) and $params['sequence']=="ASC"){
        //Формируем с убыванием
        return "?page={$params['page']}&sort={$params['sort']}&sequence=DESC";
      }else{
        //Формируем с возрастанием
        return "?page={$params['page']}&sort={$params['sort']}";
      }
    }else{
      //Если поле сортировки не пришло то ставим новое поле с базовой сортировкой
      return "?page={$params['page']}&sort={$field}";
    }
  }

  //Формирование url для пагинации
  //Приходят параметры сортировки текущей страницы
  //И новая страница, на которую нужно перейти
  public static function buildUrlOnPage($params,$page){
    $data = $params;
    $data['page'] = $page;//Говорим что теперь в параметрах новая страница
    foreach ($data as $key => $value) {
      $data[$key] = $key."=".$value;//Формируем интересный массив
      //формата ['page=1','sort=exp','seq=exp']
    }
    $data="?".implode('&',$data);//И формируем url
    return $data;
  }

  //Формирование url возвращения на базовую страницу
  //Приходят параметры предыдущей страницы
  public static function buildUrlOnBack($params){
    $data = $params;//действия аналогичны предыдущей функции
    //Только нет новой страницы
    foreach ($data as $key => $value) {
      $data[$key] = $key."=".$value;
    }
    $data="?".implode('&',$data);
    return $data;
  }
}
