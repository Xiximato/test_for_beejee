<?php
//Проверяем существование переменной, которая объявляется в index.php
defined('BASEPATH') or die("Вот так работает защита от запроса на все файлы кроме корневого index.php");

//Просто массив с данными о возможных путях и из действиях
$routes = array(
  'base_url' => 'task',
  '' => ['main','index'],
  'main' => ['main','index'],
  'main/index' => ['main','index'],
  'main/create' => ['main','create'],
  'main/read' => ['main','read'],
  'main/update' => ['main','update'],
  'main/delete' => ['main','delete'],
  'main/out' => ['main','out'],
  'account' => ['account','login'],
  'account/login' => ['account','login']
);

return $routes;

?>
