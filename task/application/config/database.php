<?php
//Проверяем существование переменной, которая объявляется в index.php
defined('BASEPATH') or die("Вот так работает защита от запроса на все файлы кроме корневого index.php");

//Просто массив с данными о подключении
$data = array(
  'host' => 'localhost',
  'username' => 'admin',
  'password' => '123',
  'dbname' => 'tasks'
);

return $data;
