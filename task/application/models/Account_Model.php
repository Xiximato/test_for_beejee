<?php
//Проверяем существование переменной, которая объявляется в index.php
defined('BASEPATH') or die("Вот так работает защита от запроса на все файлы кроме корневого index.php");

/**
 * Сущность для работы с таблицей user
 */
class Account_Model extends Model_System{

  //Получение информации о доступе пользователя
  public function getUserPerm($params = []){
    //Пробегаемся по полученным данным и тут же их экранируем
    foreach($params as $key => $value) {
      $$key = mysqli_real_escape_string($this->db->link,$value);
    }
    //Так же можно делать и в других моделях, сдела просто ради примера

    //Сначала делаем запрос на уникальный ключ username
    //В тестовом просили чтобы бы заходили по данным admin - 123 поэтому уникальный ключ username
    $sql = "SELECT username,password,status FROM user WHERE username='{$username}'";

    //Получили результат запроса
    $data = mysqli_fetch_assoc($this->db->query($sql));
    if(empty($data)){//Если он пустой
      return false;//Значит и прав у него нет, возвращаем false
    }
    $hash = $data['password'];//Получаем hash пароля
    unset($data['password']);
    //И сравниваем, если всё хорошо то возвращаем данные о пользователе или false
    return password_verify($password,$hash)?$data:false;
  }

}
