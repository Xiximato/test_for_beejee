<?php
//Проверяем существование переменной, которая объявляется в index.php
defined('BASEPATH') or die("Вот так работает защита от запроса на все файлы кроме корневого index.php");

/**
 *Модель основного контроллера
 *Осуществляет взаимодействие с базой данных
 *Формирует запросы и возвращает скомпанованные данные
 */
class Main_Model extends Model_System{

  //Функция получения текущей страницы
  //Используется методом index
  public function getCurrentPage($params = []){
    //Это сделал для того, чтобы не замарачиваться с лишними ключами
    if(!empty($params)) foreach ($params as $key => $value) $$key=$value;

    $taskLimit = 3;//Лимит заданий на странице. Можно сделать и через конфиг
    $sql = "SELECT count(*) as count FROM task";//Запрос на кол-во записей в базе
    //Округляем в большую сторону(Забираем из результата столбец count и делим на лимит заданий)
    $countPage = ceil(mysqli_fetch_assoc($this->db->query($sql))['count']/$taskLimit);
    //Сколько нужно пропустить записей прежде чем начать забирать данные
    $offset = ($page-1)*$taskLimit;

    //Кроме всего прочего у нас предусмотренна сортировка по одному из параметров
    $sql = "SELECT id,name,email,tasktext,status FROM task ";
    if(isset($sort)) $sql.="ORDER BY {$sort} {$sequence} ";//Здесь она и формируется
    $sql.="LIMIT {$offset},{$taskLimit}";

    $query = $this->db->query($sql);//Готовый sql отправляем в модель и получаем ответ

    while($row = mysqli_fetch_assoc($query)){
      $data['list'][] = $row;
    }//Здесь мы формируем лист полученных задач

    //Но может быть такое, что задачи не пришли в связи с чем нужно проверить
    if(!isset($data)) return false;//А создался ли массив data в том цикли
    //И если нет то вернём false

    //Этот цикл предназначен для обрезания строк
    foreach ($data['list'] as $key => $item){
      //Текст задач может попадаться очень большой
      $data['list'][$key]['tasktext'] = mb_substr($data['list'][$key]['tasktext'],0,30)." ...";
      //Я ещё не очень силён в стилях, и просто немного облегчил задачу с выводом
    }
    //Если закоментировать этот цикл, то текст задач будет выводиться полностью
    //Может поплзти :(

    $data['countPage'] = $countPage;//Доформировываем результат
    return $data;//и возвращаем
  }

  //Функция получения текущего задания
  //Используется методами update и read
  public function getCurrentTask($id){
    $idb = mysqli_real_escape_string($this->db->link,$id);//Экранируем все что внутри
    $sql = "SELECT * FROM task WHERE id={$idb}";//Формируем запрос
    $data = mysqli_fetch_assoc($this->db->query($sql));//Получаем одну строку
    //так как параметр id уникальный
    //В данном случае действительно понадобятся все данные из таблицы
    //Но можно заменить на перечисление столбцов
    //id,username,email,tasktext,status

    return $data;
  }

  //Функция удаления текущего задания
  //Используется методом delete
  public function deleteCurrentTask($id){
    $idb = mysqli_real_escape_string($this->db->link,$id);//Экранируем id
    $sql = "DELETE FROM task WHERE id={$idb}";//Удаляем по уникальному ключу
    return $this->db->query($sql);//Возвращаем ответ об успехе, хоть и не понадобится
  }

  //Функция изменения текущего задания
  //Используется методом update
  public function updateCurrentTask($params = []){
    //Это сделал для того, чтобы не замарачиваться с лишними ключами
    if(!empty($params)) foreach ($params as $key => $value) $$key=$value;

    //Из формы обновления может придти только 3 поля
    $idb = mysqli_real_escape_string($this->db->link,$id);//Экранируем их
    $textb = mysqli_real_escape_string($this->db->link,$text);//Текст задачи
    $statusb = mysqli_real_escape_string($this->db->link,$status);//Статус задачи
    $sql = "UPDATE task SET tasktext='{$textb}',status={$statusb}  WHERE id={$idb}";//Формируем запрос
    return $this->db->query($sql);//И отправляем отчёт об успехе, хотя и не понадобится
  }

  //Функция создания текущего задания
  //Используется методом create
  public function insertTask($params = []){
    //Это сделал для того, чтобы не замарачиваться с лишними ключами
    if(!empty($params)) foreach ($params as $key => $value) $$key=$value;

    //Из формы создания могут придти только 3 значения
    //Уникальный ключ заполниться по AUTO INCREMENT
    $nameb = mysqli_real_escape_string($this->db->link,$name);//Имя автора
    $emailb = mysqli_real_escape_string($this->db->link,$email);//Его почтта
    $textb = mysqli_real_escape_string($this->db->link,$text);//Его задача

    $sql = "INSERT INTO task(name,email,tasktext) VALUES ";//Формируем запрос
    $sql .= "('{$nameb}','{$emailb}','{$textb}')";
    return $this->db->query($sql);//Отправляем отчёт о результате, хотя и не понадобится
  }

}
