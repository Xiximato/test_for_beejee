<!DOCTYPE html>
<html lang="ru" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Редактировать задачу <?php echo $data['content']['id'];?></title>
    <link rel="stylesheet" href="/vendor/create.css"/>
  </head>
  <body>
    <form method="post" class='forming'>
      <label>Ваше имя</label><input type='text' name='name' required><br>
      <label>Ваша почта</label><input type='email' name='email' required><br>
      <label>Ваша задача</label><textarea rows="10" cols="45" name="text" required></textarea><br>
      <p><input type="submit" value="Сохранить"></p>
    </form>
    <?php
    echo "<a href='{$data['back_url']}'>Назад</a>";
    ?>
  </body>
</html>
