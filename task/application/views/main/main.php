<!DOCTYPE html>
<html lang="ru" dir="ltr">
<head>
  <meta charset="utf-8">
  <title>Главная страница</title>
  <link rel="stylesheet" href="/vendor/main.css"/>
</head>
<body>
  <div class="main">
    <div class="navigation">
      <div><a href="/<?php echo ROOT?>/main/create">Создать новую задачу</a></div>
      <?php if(isset($data['status']) and $data['status']=="admin"):?>
        <div><a href="/<?=ROOT?>/main/out">Выйти</a></div>
      <?php else:?>
        <div><a href="/<?=ROOT?>/account">Войти</a></div>
      <?php endif;?>
    </div>
    <div class="header">
      <table class="table-header">
        <tr>
          <?php if(isset($data['status']) and $data['status']=="admin"):?>
            <th class='admin'></th>
          <?php endif;?>
          <?php
          $query = QueryBuild_Helper::buildUrlOnHead($data['params'],'id');
          echo "<th class='id'><a href='/".ROOT."/main/index{$query}'>Номер</a></th>";
          $query = QueryBuild_Helper::buildUrlOnHead($data['params'],'name');
          echo "<th class='name'><a href='/".ROOT."/main/index{$query}'>Имя</a></th>";
          $query = QueryBuild_Helper::buildUrlOnHead($data['params'],'email');
          echo "<th class='email'><a href='/".ROOT."/main/index{$query}'>Почта</a></th>";
          $query = QueryBuild_Helper::buildUrlOnHead($data['params'],'tasktext');
          echo "<th class='tasktext'><a href='/".ROOT."/main/index{$query}'>Текст</a></th>";
          $query = QueryBuild_Helper::buildUrlOnHead($data['params'],'status');
          echo "<th class='status'><a href='/".ROOT."/main/index{$query}'>Статус</a></th>";
          ?>
        </tr>
      </table>
    </div>
    <div class="content">
      <table class="table-content">
        <?php for($i=0;$i<3;$i++):?>
          <tr>
            <?php if(isset($data['list'][$i])):?>
              <?php if(isset($data['status']) and $data['status']=="admin"):?>
                <td class='admin'>
                  <a href="/<?=ROOT?>/main/delete?id=<?=htmlspecialchars($data['list'][$i]['id'], ENT_QUOTES, 'UTF-8')?>">Удалить</a><br>
                  <a href="/<?=ROOT?>/main/update?id=<?=htmlspecialchars($data['list'][$i]['id'], ENT_QUOTES, 'UTF-8')?>">Изменить</a><br>
                </td>
              <?php endif;?>
                <td class='id'><a href='/<?=ROOT?>/main/read?id=<?=$data['list'][$i]['id']?>'><?=$data['list'][$i]['id']?></td>
                <td class='name'><?=htmlspecialchars($data['list'][$i]['name'], ENT_QUOTES, 'UTF-8')?></td>
                <td class='email'><?=htmlspecialchars($data['list'][$i]['email'], ENT_QUOTES, 'UTF-8')?></td>
                <td id="tasktextinfo" class='tasktext'><?=htmlspecialchars($data['list'][$i]['tasktext'], ENT_QUOTES, 'UTF-8')?></td>
                <?php if($data['list'][$i]['status']==1):?>
                  <td class='status'>Ready</td>
                <?php else:?>
                  <td class='status'>In procces</td>
                <?php endif;?>
              <?php endif;?>
            </tr>
          <?php endfor;?>
        </table>
      </div>
      <div class='pages'>
        <?php
        if(isset($data['countPage'])){
          for($i=1;$i<=$data['countPage'];$i++){
            $query = QueryBuild_Helper::buildUrlOnPage($data['params'],$i);
            echo "<a class='choose-page' href='/".ROOT."/main/index{$query}'>{$i}<a>";
          }
        }
        ?>
      </div>
    </div>
  </body>
  </html>
