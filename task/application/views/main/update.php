<!DOCTYPE html>
<html lang="ru" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Редактировать задачу <?php echo $data['content']['id'];?></title>
  </head>
  <body>
    <div class="header">
      <h1><?php echo htmlspecialchars($data['content']['name'], ENT_QUOTES, 'UTF-8'); ?></h1>
      <h2><?php echo htmlspecialchars($data['content']['email'], ENT_QUOTES, 'UTF-8'); ?></h2>
    </div>
    <form method="post">
      <div class="text">
        <textarea rows="10" cols="45" name="text">
        <?php echo htmlspecialchars($data['content']['tasktext'], ENT_QUOTES, 'UTF-8'); ?>
        </textarea>
      </div>
      <div class="foot">
        <?php if($data['content']['status']==0):?>
          <input type='checkbox' name='status'>
        <?php else:?>
          <input type='checkbox' name='status' checked>
        <?php endif;?>
      </div>
      <p><input type="submit" value="Сохранить"></p>
    </form>
    <?php
    echo "<a href='{$data['back_url']}'>Назад</a>";
    ?>
  </body>
</html>
