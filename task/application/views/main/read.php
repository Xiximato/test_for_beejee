<!DOCTYPE html>
<html lang="ru" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Прочитать задание номер <?php echo $data['content']['id'];?></title>
  </head>
  <body>
    <div class="header">
      <h1><?php echo htmlspecialchars($data['content']['name'], ENT_QUOTES, 'UTF-8'); ?></h1>
      <h2><?php echo htmlspecialchars($data['content']['email'], ENT_QUOTES, 'UTF-8'); ?></h2>
    </div>
    <div class="text">
      <textarea rows="30" cols="100" name="text">
      <?php echo htmlspecialchars($data['content']['tasktext'], ENT_QUOTES, 'UTF-8'); ?>
      </textarea>
    </div>
    <div class="foot">
      <?php if($data['content']['status']==0):?>
        <h3>Невыполнено</h3>
      <?php else:?>
        <h3>Выполнено</h3>
      <?php endif;?>
    </div>
    <?php
    echo "<a href='{$data['back_url']}'>Назад</a>";
    ?>
  </body>
</html>
