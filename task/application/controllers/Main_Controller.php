<?php
//Проверяем существование переменной, которая объявляется в index.php
defined('BASEPATH') or die("Вот так работает защита от запроса на все файлы кроме корневого index.php");

/**
*Основной котнорллер
*/
class Main_Controller extends Controller_System{

  function __construct($name){
    parent::__construct($name);//Сразу передаём в системный контроллер информацию о нашем контроллере
    $this->load_model('main');//Подгружаем модель
    //Теперь можно обращаться через имя main
  }

  //Основной метод
  //На вход может принимать множество параметров
  //Страница, поле сортировки,последовательность
  function indexAction($params = []){
    //Это сделал для того, чтобы не замарачиваться с лишними ключами
    if(!empty($params)) foreach ($params as $key => $value) $$key=$value;

    //Узнаём пришли ли нам наши параметры а их всего 3 может быть
    if(!isset($page)) $page=1;//Страница пагинации
    if(!isset($sort)) $sort="id";//Поле по которому сортируем
    if(!isset($sequence)) $sequence="ASC";//Последовательность сортировки

    $params = array(
      'page' => $page,
      'sort' => $sort,
      'sequence' => $sequence
    );//Формируем массив для отправки в форму

    //И запоминаем его в сессии, в дальнейшем пригодиться
    $_SESSION['params'] = json_encode($params);

    //Обращаемся к моделе и получем текущий список задач с пагинацией
    $data = $this->main->getCurrentPage($params);

    //Проверяем на пустоту
    if(empty($data) and $page!=1){//Если он пустой,и стрница не равна 1
      $params = array(//То сформируем новый массив
        'page' => ($page-1),
        'sort' => $sort,
        'sequence' => $sequence
      );
      //Сформируем url пути на предыдущую страницу
      $back_url = "/".ROOT."/main/index".QueryBuild_Helper::buildUrlOnBack($params);
      //Отправимся туда
      View_System::redirect($back_url);
      //Данный виток необходим при ситуации
      //Когда на странице с одним задание удалили последнюю задачу
      //И страница осталась пуста
      //Чтобы не сидеть и не глазеть перейдём на страницу назад
    }

    //Если же все хорошо и данные есть
    $data['params'] = $params;//Компануем их параметрами страницы
    //Понадобиться для формирование url

    //Далее проверяем статус нашего пользователя
    //На данные момент есть смысл заходить только админом
    //Другие пользователи могут только создавать и читать форму, как и гости
    if(isset($_SESSION['authentication']) and $_SESSION['authentication']=='admin'){
      $data['status'] = $_SESSION['authentication'];
    }
    //Отобрадаем страницу
    $this->view->render('main',$data);
  }

  //Метод чтения события
  //Принимает только id задачи которую хотим прочитать
  function readAction($params = []){
    //Это сделал для того, чтобы не замарачиваться с лишними ключами
    if(!empty($params)) foreach ($params as $key => $value) $$key=$value;

    //При доступе к сессии мы знаем какие были предыдущие параметры страницы
    //Благодаря чем формируем utl возвращения
    $data['params'] = json_decode($_SESSION['params'],true);
    $data['back_url'] = "/".ROOT."/main/index".QueryBuild_Helper::buildUrlOnBack($data['params']);

    //Если вдруг id не задался
    //Возможно кто-то захотел побаловаться через url и ввёл неправильно то его выкенет обратно
    if(!isset($id)){
      View_System::redirect($data['back_url']);
    }

    //Получив информацию о задаче
    $data['content'] = $this->main->getCurrentTask($id);
    //Здесь тоже можно сделать проверку!!!

    //Отправляемся с ней на страницу чтения
    $this->view->render('read',$data);

  }

  //Метод выхода из аккаунта
  function outAction(){
    //Говорим, что тепер на сайте нет авторитизированного юзера
    unset($_SESSION['authentication']);
    //И возвращаем страницу в исходное состояние.
    //Но тут можно сохранить состояния страниц, если сформировать обратный url
    View_System::redirect("/".ROOT."/main/index");
  }

  //Метод удаления конкретной записи
  function deleteAction($params = []){
    //Это сделал для того, чтобы не замарачиваться с лишними ключами
    if(!empty($params)) foreach ($params as $key => $value) $$key=$value;

    //Опять же спасает SESSION зная какие были значения предыдущей страницы
    $data['params'] = json_decode($_SESSION['params'],true);
    $data['back_url'] = "/".ROOT."/main/index".QueryBuild_Helper::buildUrlOnBack($data['params']);
    //Формируем url для возврата

    //Здесь мы проверяем а был ли введён id
    if(!isset($id)){
      View_System::redirect($data['back_url']);
    }
    //А так же, проверяем пользователя на админа!!! Иначе можно удалять через url
    //Или при двух страницах с одной выйти, а на другой всё ещё иметь возможнсоть удалять
    $this->checkPrem();

    //Если же все хорошо отправляем запрос на удаление
    $this->main->deleteCurrentTask($id);
    View_System::redirect($data['back_url']);//И возвращаемся на страницу с такими же параметрами
  }

  //Метод обновления конкретной записи
  function updateAction($params = []){
    //Это сделал для того, чтобы не замарачиваться с лишними ключами
    if(!empty($params)) foreach ($params as $key => $value) $$key=$value;

    //Формируем обратный url
    $data['params'] = json_decode($_SESSION['params'],true);
    $data['back_url'] = "/".ROOT."/main/index".QueryBuild_Helper::buildUrlOnBack($data['params']);
    //Данный код можно запихнуть в отдельную функцию
    //В моделе создавать не хотел лишнюю
    //А мой Helper подошёл бы
    //Но при желании эти строчки можно скомпоновать до
    //$data['back_url'] = "/".ROOT."/main/index".QueryBuild_Helper::buildUrlOnBack(json_decode($_SESSION['params'],true));

    //Далее идёт главное распределение ролей этого метода
    if(isset($_POST['text'])){//Если мы видим форму, значит пользовател уже корректировал значения
      $upd['text'] = $_POST['text'];//Из данныой формы мы можем получить только параметры текста и статуса
      if(isset($_POST['status'])){//Так как статус в базе прописан как булевый параметр то и обработать нужно подобающе
        $upd['status'] = 1;
      }else{
        $upd['status'] = 0;
      }
      $upd['id'] = $id;//Дополняем массив данных

      //И здесь ситуация дублируется
      if(!isset($id)){
        View_System::redirect($data['back_url']);
      }

      //Если кто-то вышел уже из аккаунта то и обновлять он не может
      $this->checkPrem();

      //Если всё хорошо то отправили запрос на обновление
      $this->main->updateCurrentTask($upd);
      //View_System::redirect($data['back_url']);//И возвращаемся
    }else{
      //И здесь ситуация дублируется
      if(!isset($id)){
        View_System::redirect($data['back_url']);
      }
      //Не стал выносить в отдельную функцию, передавать целый массив, чтобы проверить одну переменную?
      
      //Если кто-то вышел уже из аккаунта то и обновлять он не может
      $this->checkPrem();

      //Если всё хорошо то идём заполнять форму
      $data['content'] = $this->main->getCurrentTask($id);//Подгружаем данные по форме которые уже были у этого объекта
      $this->view->render('update',$data);
    }
  }

  //Проверяет является ли пользователь всё ещё авторитизированным или нет
  function checkPrem(){
    if(!isset($_SESSION['authentication']) and $_SESSION['authentication']!='admin'){
      View_System::redirect("/".ROOT."/account");
    }
  }

  //Метод создания задачи
  function createAction(){
    //Уже не первый раз встречаем
    $data['params'] = json_decode($_SESSION['params'],true);
    $data['back_url'] = "/".ROOT."/main/index".QueryBuild_Helper::buildUrlOnBack($data['params']);
    //Уже не первый раз встречаем

    //На самом деле эту форму можно объединить с updatom так как выполняют по сути одно и тоже
    //Проверяют пришла ли форма и отпраляют куда понадобится
    //А форма на update и create может быть одинаковой
    if(isset($_POST['text'])){//Проверяем пришла ли форма
      //id будет автоматическим
      $upd['name'] = $_POST['name'];//Если да, то без проблем забираем данные
      $upd['email'] = $_POST['email'];
      $upd['text'] = $_POST['text'];
      //статус по дефолту невыполнен

      //Отправляем запрос на добавление
      $this->main->insertTask($upd);
      View_System::redirect($data['back_url']);//И возвращаемся на страницу
    }else{
      //А иначе просто идём заполнять форму
      $this->view->render('create',$data);
    }
  }
}
