<?php
//Проверяем существование переменной, которая объявляется в index.php
defined('BASEPATH') or die("Вот так работает защита от запроса на все файлы кроме корневого index.php");

/**
*Сущность работы с аккаунтом
*/
class Account_Controller extends Controller_System{

  function __construct($name){
    parent::__construct($name);//Передаётся во View
    $this->load_model('account');//Подгружаем модель
    //Можно обращаться через account
  }

  //Метод проверки вход от пользователя
  public function loginAction(){
    //Если пришёл ответ значит пытался зайти
    if(isset($_POST['username'])){
      $params['username'] = $_POST['username'];//Смотрим его несчастные попытки
      $params['password'] = $_POST['password'];//Смотрим его несчастные попытки

      //Отправляем данные и проверяем их
      //Получаем ответ либо false либо данные о пользователе
      $data = $this->account->getUserPerm($params);
      if($data){//Если не пустой
        $_SESSION['authentication'] = "admin";//Поздравляем, вы админ!!!
        View_System::redirect("/".ROOT."/main/index");//Идите изменять, что хотите
      }else{//Иначе
        $data['try'] = "false";//Извини но попытайся снова
        $this->view->render("login",$data);//Отправляем на форму с коментарием о провале
      }
    }else{
      //Зашёл впервые, так и вводи свои данные
      $this->view->render('login');
    }

    //Из маленьких минусов не доделал кнопку возврата на главную страницу
  }

}
