В данном проекте я реализовал небольшое mvc приложение

По чистому времени данная разработка заняла 23 часа
Из них большая часть времени ушла на изучение структуры mvc
Так-же были неудачные попытки, после которых переписывал всю структура заново

На финальную версию разработки ушло 3 часа

К проекту прикладываю sql базы данных

Из багов которые не успел исправить
При изменении админом задачи, текст в поле смещается на несколько tab-ов
Из-за чего при нескольких обновлениях сначала будет стоять куча пробелов
Потом идёт текст

Пароль в базе лежит в hash-рованом виде и при входе идёт сравнение через
стандартную функцию php password_verify

В форме входа не хватает кнопки, вернуться назад.

При выкладывании на хост возникали противоречия путей
Поэтому сайт который находится на бесплатном хосте совсем немного видоизменён
В основном только там, где необходимо было указывать пути
